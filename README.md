# telegramBot

# Install requirement following below command

pip3 install -r requirements.txt

# You have to create migrate folder and init elembic 
# It will automatically detect your database  

alembic revision --autogenerate -m "Added account table"

# Using alembic upgrade command to map field

alembic upgrade head

# Learn more about below documents
https://alembic.sqlalchemy.org/en/latest/autogenerate.html

# In config file, you have to create path to folder and url engine to your database
https://docs.sqlalchemy.org/en/14/core/engines.html#mysql

# Learn more about celery and rabbitmq to using virtual host and connect via amqp
