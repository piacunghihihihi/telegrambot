import unittest
from unittest.mock import patch
from marshmallow import ValidationError
from app.models.image import Images
from app.services.image_service import delete_image, get_single_image, post_image, patch_image
from app.exception.exception_api import (
    AlreadyExistedException,
    NoInputException,
    NotFoundException,
)


class TestLogicApi(unittest.TestCase):
    def test_images(self):
        fake_image_get = {
            "id": "1c17eb63-25e4-4f9f-8076-f148822a4e14",
            "is_deleted": False,
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg",
            "name": "ben-sweet-2LowviVHZ-E-unsplash-1.jpeg",
            "update_at": "Mon, 30 Aug 2021 17:00:41 GMT",
        }
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.all.return_value = (
                fake_image_get
            )

            obj = mocked_object()
            mock_query = obj.query.return_value.all.return_value

        self.assertEqual(mock_query, fake_image_get)

    def test_image_not_found_with_get_image(self):
        fake_id_get = "51b99358-6b25-4663-b473-ce6315bc38f4"
        image_model = Images
        image_model.id = fake_id_get
        fake_exception = "Image not found"
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.query.return_value.filter_by.return_value.first.return_value = (
                None
            )

            with self.assertRaises(NotFoundException) as context:
                get_single_image(fake_id_get)

        self.assertEqual(fake_exception, str(context.exception))
        self.assertEqual(image_model.id, fake_id_get)

    def test_image_get_success(self):
        fake_image_get = {
            "id": "1c17eb63-25e4-4f9f-8076-f148822a4e14",
            "is_deleted": False,
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg",
            "name": "ben-sweet-2LowviVHZ-E-unsplash-1.jpeg",
            "update_at": "Mon, 30 Aug 2021 17:00:41 GMT",
        }
        fake_id_get = "12323323asdas55dsasd12312"
        image_model = Images
        image_model.id = fake_id_get
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                fake_image_get
            )

            obj = mocked_object()
            mock_query = (
                obj.query.return_value.filter_by.return_value.first.return_value
            )

        self.assertEqual(mock_query, fake_image_get)
        self.assertEqual(image_model.id, fake_id_get)

    def test_no_payload_with_post_image(self):
        data_payload = None
        fake_exception = "No input data provided"
        with patch("app.services.image_service.post_image") as mocked_object:
            mocked_object.return_value.req_data.return_value = None

            with self.assertRaises(NoInputException) as context:
                post_image(data_payload)

            self.assertEqual(fake_exception, str(context.exception))

    def test_no_data_provided_in_field_with_post_image(self):
        data_payload = {
            "is_deleted": False,
            "name": "ben-sweet-2LowviVHZ-E-unsplash-123.jpeg",
            "location": "",
        }
        fake_exception = str({"location": ["Data not provided"]})
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                None
            )

            with self.assertRaises(ValidationError) as context:
                post_image(data_payload)
            self.assertEqual(fake_exception, str(context.exception))

    def test_pass_wrong_argument_with_post_image(self):
        data_payload = {
            "is_deleted": False,
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-1234.jpeg",
            "name": "ben-sweet-2LowviVHZ-E-unsplash-1.jpeg",
            "address": "Ha noi",
        }
        fake_exception = str({"address": ["Unknown field."]})
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                data_payload
            )

            with self.assertRaises(ValidationError) as context:
                post_image(data_payload)
            self.assertEqual(fake_exception, str(context.exception))

    def test_missing_field_with_post_image(self):
        data_payload = {
            "is_deleted": False,
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-1234.jpeg",
        }
        fake_exception = str({"name": ["Missing data for required field."]})
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                None
            )

            with self.assertRaises(ValidationError) as context:
                post_image(data_payload)
            self.assertEqual(fake_exception, str(context.exception))

    def test_image_already_existed_with_post_image(self):
        data_payload = {
            "is_deleted": False,
            "name": "ben-sweet-2LowviVHZ-E-unsplash-123.jpeg",
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-123.jpeg",
        }
        fake_exception = "Image already existed"
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                None
            )

            with self.assertRaises(AlreadyExistedException) as context:
                post_image(data_payload)
            self.assertEqual(fake_exception, str(context.exception))

    def test_image_post_success(self):
        fake_image_get = {
            "is_deleted": False,
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-2.jpeg",
            "name": "ben-sweet-2LowviVHZ-E-unsplash-2.jpeg",
        }
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                fake_image_get
            )

            obj = mocked_object()
            mock_query = (
                obj.query.return_value.filter_by.return_value.first.return_value
            )

        self.assertEqual(mock_query, fake_image_get)

    def test_no_data_provided_with_patch_image(self):
        data_id = "63884316-8c40-426b-b6a8-bcd0b6c61077"
        req_data = None
        fake_exception = "No input data provided"
        with patch("app.services.image_service.session") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                None
            )

            with self.assertRaises(NoInputException) as context:
                patch_image(data_id, req_data)
            self.assertEqual(fake_exception, str(context.exception))

    def test_not_found_with_patch_image(self):
        data_id = "63884316-8c40-426b-b6a8"
        req_data_payload = {
            "name": "ben-sweet-2LowviVHZ-E-unsplash-123.jpeg",
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-123.jpeg",
        }
        fake_exception = "Image not found"
        with patch("app.services.image_service.patch_image") as mocked_object:
            mocked_object.return_value.req_data = req_data_payload

            with self.assertRaises(NotFoundException) as context:
                patch_image(data_id, req_data_payload)
            self.assertEqual(fake_exception, str(context.exception))

    def test_image_patch_success(self):
        fake_image_get = {
            "is_deleted": False,
            "location": "/home/long/telegrambot/IMAGE/ben-sweet-2LowviVHZ-E-unsplash-2.jpeg",
            "name": "ben-sweet-2LowviVHZ-E-unsplash-2.jpeg",
        }
        with patch("app.services.image_service.patch_image") as mocked_object:
            mocked_object.return_value.req_data = fake_image_get

            obj = mocked_object()
            mock_query = obj.req_data
        self.assertEqual(mock_query, fake_image_get)

    def test_not_found_with_delete_image(self):
        data_id = "63884316-8c40-426b-b6a8"

        fake_exception = "Image not found"
        with patch("app.services.image_service.delete_image") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                1
            )

            with self.assertRaises(NotFoundException) as context:
                delete_image(data_id)
            self.assertEqual(fake_exception, str(context.exception))

    def test_image_delete_success(self):
        fake_id = "63884316-8c40-426b-b6a8-bcd0b6c61077"
        with patch("app.services.image_service.delete_image") as mocked_object:
            mocked_object.return_value.query.return_value.filter_by.return_value.first.return_value = (
                fake_id
            )

            obj = mocked_object()
            mock_query = (
                obj.query.return_value.filter_by.return_value.first.return_value
            )
        self.assertEqual(mock_query, fake_id)


if __name__ == "__main__":
    unittest.main()
