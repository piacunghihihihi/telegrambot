#!/bin/sh

# wait for RabbitMQ server to start
sleep 5

# run Celery worker for our project myproject with Celery configuration stored in Celeryconf
su -m sh_run -c "celery -A app.services.worker_service worker --loglevel=INFO --concurrency=10 -n worker1@%h"
