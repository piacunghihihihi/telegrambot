from sqlalchemy.sql.schema import ForeignKey
from marshmallow import Schema, fields, ValidationError
from app.models.db_init import Base
from app.models.image import ImageSchema
from sqlalchemy import Column, String
from uuid import uuid4

class Logs(Base):

    __tablename__ = 'logs'

    id = Column(String(length=36), default=lambda: str(uuid4()), primary_key=True)
    image_id = Column(String(length=36), ForeignKey('images.id'))  
    action = Column(String(50))


def log_not_be_blank(data):
    if not data:
        raise ValidationError("Data not provided")


class LogsSchema(Schema):
    id = fields.Str()
    logs = fields.Nested(ImageSchema, validate=log_not_be_blank)
    action = fields.Str(validate=log_not_be_blank)

logs_schema = ImageSchema(many=True)
log_schema = ImageSchema()