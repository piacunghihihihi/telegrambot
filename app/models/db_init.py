from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker
from app.services.config_env import env


Base = declarative_base()

engine = create_engine(env.str('MYSQLDB'), echo = True)

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()