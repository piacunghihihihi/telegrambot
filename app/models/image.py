from sqlalchemy.sql.sqltypes import Boolean, DateTime, String
from marshmallow import Schema, fields, ValidationError
from sqlalchemy.orm import relationship
from app.models.db_init import Base
from sqlalchemy import Column, String
from uuid import uuid4
import datetime



class Images(Base):

    __tablename__ = 'images'

    id = Column(String(length=36), default=lambda: str(uuid4()), primary_key=True)
    name = Column(String(length=100), nullable=True)
    location = Column(String(length=100),  nullable=True)
    update_at = Column(DateTime, default=datetime.datetime.utcnow)
    is_deleted = Column(Boolean, unique=False)
    logs = relationship('Logs', backref='image')
    

def image_not_be_blank(data):
    if not data:
        raise ValidationError("Data not provided")


class ImageSchema(Schema):
    id = fields.Str()
    name = fields.Str(required=True, validate=image_not_be_blank)
    location = fields.Str(required=True, validate=image_not_be_blank)
    update_at = fields.DateTime(dump_only=True)
    is_deleted = fields.Boolean(required=True)


images_schema = ImageSchema(many=True)
image_schema = ImageSchema()