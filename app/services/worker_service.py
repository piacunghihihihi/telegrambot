
from celery import Celery
import requests
from app.models.db_init import session
from app.models.log import Logs
from app.models.image import Images
import os 
from app.services.config_env import env

celery = Celery('tasks', broker=env.str('AMQP'))


@celery.task(bind=True, default_retry_delay=1 * 60)
def get_row(self, data):
    try:
        query_image = session.query(Images).filter_by(id=data['id']).first()
        add_log = Logs(image=query_image, action=data['status'])
        session.add(add_log)
        session.commit()
    except Exception as err:
        raise self.retry(exc=err, countdown=1 * 60)



@celery.task(bind=True, default_retry_delay=1 * 60)
def upload_image(self, data):
    try:
        list_image_file = [os.path.join(env.str('SCANPATH'), entry) for entry in os.listdir(env.str('SCANPATH'))]  # get name, url
        for image in list_image_file:
            if data['location'] == image:
                files = {
                        'photo': open(data['location'], 'rb')
                    }
                message = ('https://api.telegram.org/bot'+ env.str('BOT_TOKEN') + '/sendPhoto?chat_id=' + env.str('CHAT_ID'))
                send = requests.post(message, files = files)
    except Exception as err:
        raise self.retry(exc=err, countdown=1 * 60)