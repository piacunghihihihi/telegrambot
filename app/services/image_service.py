import uuid
from app.models.image import Images
from app.models.log import Logs
from app.models.db_init import session
from app.models.image import image_schema, images_schema
from app.exception.exception_api import (
    NoInputException,
    NotFoundException,
    AlreadyExistedException,
)


def get_images():
    images_query = session.query(Images).all()
    images = images_schema.dump(images_query)
    return images


def post_image(req_data):
    if not req_data:
        raise NoInputException("No input data provided")

    image = image_schema.load(req_data)

    image_query = session.query(Images).filter_by(location=image["location"]).first()
    if image_query:
        raise AlreadyExistedException("Image already existed")

    image_id = image.get("id")
    if not image_id:
        image_id == uuid.uuid4().hex

    image_post_db = Images(
        id=image_id,
        name=image["name"],
        location=image["location"],
        is_deleted=image["is_deleted"],
    )
    session.add(image_post_db)
    session.commit()

    return image_post_db


def get_single_image(id):
    image_query_id = session.query(Images).filter_by(id=id).first()

    if not image_query_id:
        raise NotFoundException("Image not found")

    return image_query_id


def patch_image(id, req_data):
    if not req_data:
        raise NoInputException("No input data provided")

    image_query_id = session.query(Images).filter_by(id=id).first()

    if not image_query_id:
        raise NotFoundException("Image not found")

    for key, value in req_data.items():
        setattr(image_query_id, key, value)

    session.add(image_query_id)
    session.commit()

    return image_query_id


def delete_image(id):
    image_query_id = session.query(Images).filter_by(id=id).first()

    if not image_query_id:
        raise NotFoundException("Image not found")

    logs = session.query(Logs).filter_by(image_id=id).all()

    for log in logs:
        session.delete(log)
        session.commit()

    session.delete(image_query_id)
    session.commit()


    return image_query_id
