import schedule
import os
from app.models.image import Images
from app.models.db_init import session
import xlsxwriter
from app.services.config_env import env


def set_deleted_file(entries):
    key_image_file = [os.path.join(env.str('SCANPATH'), entry) for entry in entries]

    location_info_db = [row.location for row in session.query(Images).all()]
    #print("location_info_db",  location_info_db)

    excess_location = set(location_info_db) - set(key_image_file)
    for image in excess_location:
        if image in location_info_db:
            db_location = session.query(Images).filter_by(location=image).first()
            if db_location.is_deleted == False:
                db_location.is_deleted = True
                session.commit()


def add_to_db(entries):
    path_info = [(row.name, row.location) for row in session.query(Images).all()]
    list_image_file = [
        (entry, os.path.join(env.str('SCANPATH'), entry)) for entry in entries
    ]  # get name, url
    excess_name_location = set(list_image_file) - set(path_info)
    convert_list_location = list(excess_name_location)

    if len(path_info) >= 0:
        for input_name, input_url in convert_list_location:
            data_image_append = Images(
                name=input_name, location=input_url, is_deleted=False
            )
            session.add(data_image_append)
            session.commit()
    elif len(convert_list_location) == 0:
        return
    session.rollback()

def write_to_excel():
    workbook = xlsxwriter.Workbook('DataImage.xlsx')
    worksheet = workbook.add_worksheet()

    query_images = session.query(Images).all()

    row = 1
    col = 0
    for image in query_images:
        worksheet.write(0,0, "STT")
        worksheet.write(0,1, "Time")
        worksheet.write(0,2, "Location")
        worksheet.write(row, col, row)
        worksheet.write(row, col + 1, image.update_at)
        worksheet.write(row, col + 2, image.location)
        row += 1 
    workbook.close()

def entry_point():
    entries = os.listdir(env.str('SCANPATH'))
    add_to_db(entries)
    set_deleted_file(entries)
    #write_to_excel()

schedule.every(5).seconds.do(entry_point)

while True:
    schedule.run_pending()