class NoInputException(Exception):
    pass


class NotFoundException(Exception):
    pass


class AlreadyExistedException(Exception):
    pass
