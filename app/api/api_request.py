
from flask_restful import Resource, Api, abort
from flask import request
from marshmallow import ValidationError
from app.models.image import Images
from app.models.db_init import session
from app.services.worker_service import get_row, upload_image
from app.models.image import image_schema
from app.exception.exception_api import (
    AlreadyExistedException,
    NoInputException,
    NotFoundException,
)
from app.services.image_service import (
    get_images,
    post_image,
    get_single_image,
    patch_image,
    delete_image,
)


api = Api()


class ImageList(Resource):
    def get(self):
        images = get_images()
        return {"images": images}, 200

    def post(self):
        req_data = request.get_json()

        try:
            image_post = post_image(req_data)
        except NoInputException:
            return abort(400, message="No input data provided")
        except ValidationError as err:
            return abort(422, message=err.messages)
        except AlreadyExistedException:
            return abort(400, message="Image already existed")

        image_posted = image_schema.dump(session.query(Images).get(image_post.id))
        resp = request.method
        data_get_row = {"id": image_post.id, "status": resp}
        get_row.delay(data_get_row)
        upload_image.delay(req_data)

        return {"message": "Created new image.", "image": image_posted}, 201


class Image(Resource):
    def get(self, id):
        try:
            image = get_single_image(id)
        except ValidationError as err:
            return abort(422, message=err.messages)
        except NotFoundException:
            return abort(404, message="Image not found")

        image_get = image_schema.dump(image)

        resp = request.method
        data = {"id": image.id, "status": resp}
        get_row.delay(data)
        return {"image": image_get}, 200

    def patch(self, id):
        req_data = request.get_json()
        try:
            image_query_id = patch_image(id, req_data)
        except NoInputException:
            return abort(400, message="No input data provided")
        except NotFoundException:
            return abort(404, message="Image not found")

        resp = request.method
        data = {"id": image_query_id.id, "status": resp}
        get_row.delay(data)
        return {"message": "Successfully updated"}, 200

    def delete(self, id):
        try:
            image_query_id = delete_image(id)
        except NotFoundException:
            return abort(404, message="Image not found")

        resp = request.method
        data = {"id": image_query_id.id, "status": resp}
        get_row.delay(data)

        return {"message": "Successfully deleted"}, 204


api.add_resource(ImageList, "/api/v1/images")
api.add_resource(Image, "/api/v1/image/<id>")