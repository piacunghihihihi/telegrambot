from flask import Flask
from app.api.api_request import api
from app.services.worker_service import celery


def create_app():
    #Configue app
    app = Flask(__name__)

    #Configure api
    api.init_app(app)

    #config celery
    celery.conf.update(app.config)

    return app