from app.main import create_app
from app.services.worker_service import env


app = create_app()

if __name__ == "__main__":
    app.secret_key = env.str("SECRET_KEY")
    app.run(debug=True, port=5000)#, host="0.0.0.0"
