# syntax=docker/dockerfile:1
FROM python:3.8-slim-buster

ENV PYTHONPATH=$PWD/..

COPY requirements.txt requirements.txt
RUN apt-get -y update
RUN apt-get install -y python3-dev\
                    default-libmysqlclient-dev\
                    build-essential

RUN pip3 install -r requirements.txt
RUN adduser --disabled-password --gecos '' sh_run

COPY . .
